<?php

namespace App\Http\Controllers\API;
use App\User;
use App\Profile;
use Storage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;
class AuthController extends Controller
{
    public function register(Request $request)
    {
        $registerData = $request->all();
        $validator = Validator::make($registerData, [
            'name' => 'required|max:55',
            'email' => 'email|required|unique:users',
            'password' => 'required|confirmed'
        ]);
        if($validator->fails()){
            return response()->json(['isOk'=>false,'message' => $validator->errors()->first()]);
        }
        else{
        $user = User::create([
            'name'     => $request->name,
            'email'    => $request->email,
            'password'=>bcrypt($request->password),    
       ]);
        if($user!=null)
        {
        $accessToken = $user->createToken('authToken')->accessToken;
        $profile=Profile::Create([
            'user_id' => $user->id,
            'avatar'  => "public/uploads/avatars/$user->id.jpg",
        ]);
        $file = new \SplFileInfo(base_path('default.jpg'));
        Storage::putFileAs('public/uploads/avatars', $file, "$user->id.jpg");
        return response()->json(['isOk' => true, 'user' => $user, 'profile'=>$profile, 'access_token' => $accessToken]);
        }
        else{
            return response()->json([ 'isOk' => false, 'message' => 'User has not created']);
        }
        }

    }

    public function login(Request $request)
    {
        $loginData = $request->all();

        $validator = Validator::make($loginData, [
            'email' => 'email|required',
            'password' => 'required'
        ]);
        
        if($validator->fails()){
            return response()->json(['isOk'=>false,'message' => $validator->errors()->first()]);
        }

        if (!auth()->attempt($loginData)) {
            return response()->json(['isOk'=>false,'message' => 'Invalid Credentials']);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;
       
        return response()->json(['isOk'=>true,'user' => auth()->user(),'profile'=>auth()->user()->Profile()->first(), 'access_token' => $accessToken]);

    }
   
}
