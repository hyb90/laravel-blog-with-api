<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Validator;
use Storage;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post=Post::all();
        if($post==null){
            return response()->json([
                'isOk' => false,
                'message' => 'Post can not be Retrieved'
            ]); 
        }
        elseif($post->count()==0){
            return response()->json([
                'isOk' => true,
                'message' => 'No Post to Retrieve'
            ]);
        }
        else{
            return response()->json([
                'isOk' => true,
                'data' => $post
            ]);
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $PostData = $request->all();
        $validator = Validator::make($PostData, [
            'title'       => 'required|max:100',
            'content'     => 'required',
            'category_id' => 'required'
        ]);
        
        if($validator->fails()){
            return response()->json(['isOk'=>false,'message' => $validator->errors()->first()]);
        }
        else{
        $user_id=auth()->id();
        $post= Post::create([
            'user_id'     => $user_id,
            'title'       => $request->title,
            'featured'    => "public/uploads/posts/$user_id.png",
            'content'     => $request->content,
            'category_id' => $request->category_id,
            'slug'        => str_slug($request->title)
       ]);
      
       $file = new \SplFileInfo(base_path('default-post.png'));
        Storage::putFileAs('public/uploads/posts', $file, "$user_id.png");
       
        if($post!=null)
        {
        
        return response()->json(['isOk' => true, 'Post' => $post]);
        }
        else{
            return response()->json([ 'isOk' => false, 'message' => 'Post has not created']);
        }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
 
        if (!$post) {
            return response()->json([
                'isOk' => false,
                'message' => 'Post not found '
            ], 400);
        }
        else{
        return response()->json([
            'isOk' => true,
            'data' => $post->toArray()
        ], 200);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        if (!$post) {
            return response()->json([
                'isOk' => false,
                'message' => 'Post not found '
            ], 400);
        }
        else{
            $PostData = $request->all();
        $validator = Validator::make($PostData, [
            'title'       => 'required|max:100',
            'content'     => 'required',
            'category_id' => 'required'
        ]);
        
        if($validator->fails()){
            return response()->json(['isOk'=>false,'message' => $validator->errors()->first()]);
        }
        else{
        $user_id=auth()->id();
        $count= $post->Update([
            'user_id'     => auth()->id(),
            'title'       => $request->title,
            'featured'    => "public/uploads/posts/$user_id.png",
            'content'     => $request->content,
            'category_id' => $request->category_id,
            'slug'        => str_slug($request->title)
       ]);
        if($count!=0)
        {
        
        return response()->json(['isOk' => true,'message' => 'Post has Updated', 'Post' => $post]);
        }
        else{
            return response()->json([ 'isOk' => false, 'message' => 'Post has not Updated']);
        }
        }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
 
        if (!$post) {
            return response()->json([
                'isOk' => false,
                'message' => 'Post not found'
            ], 400);
        }
 
        if ($post->delete()) {
            return response()->json([
                'isOk' => true,
                'message' => 'Post has deleted'
            ]);
        } else {
            return response()->json([
                'isOk' => false,
                'message' => 'Post can not be deleted'
            ], 500);
        }
    }
}
