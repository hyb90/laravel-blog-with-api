<?php
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
  public function run()
  {
    App\Category::create([
      'name' => 'Swift',
    ]);
    App\Category::create([
      'name' => 'React',
    ]);
    App\Category::create([
      'name' => 'Vue js',
    ]);
    App\Category::create([
      'name' => 'Laravel',
    ]);
    App\Category::create([
      'name' => 'Flutter',
    ]);
    App\Category::create([
      'name' => 'Video Tutorials',
    ]);
  }
}