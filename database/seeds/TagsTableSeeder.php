<?php
use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
  public function run()
  {
    App\Tag::create([
      'tag' => 'Dart',
    ]);
    App\Tag::create([
      'tag' => 'Flutter',
    ]);
    App\Tag::create([
      'tag' => 'Android',
    ]);
    App\Tag::create([
      'tag' => 'iOS',
    ]);
    App\Tag::create([
      'tag' => 'Mobile',
    ]);
    App\Tag::create([
      'tag' => 'Kotlin',
    ]);
    App\Tag::create([
      'tag' => 'java',
    ]);
    App\Tag::create([
      'tag' => 'PHP',
    ]);
  }
}
