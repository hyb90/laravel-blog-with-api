<?php
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
  public function run()
  {
    App\Setting::create([
      'site_name'       => 'Haykal Blog System',
      'contact_phone'   => '0993998470',
      'contact_email'   => 'admin@haykal.com',
      'contact_address' => '12345 Damascus , Syria',
      'hours'           => '9am - 5pm',
      'support'         => '0993998470',
      'about'           => 'Haykal Blog System.',
    ]);
  }
}
